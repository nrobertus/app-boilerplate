set CONFIGFILE=config.xml
set CONFIGFILECOPY=www\config.xml
set ZIPFILE=build.zip

if exist C:\ProgramData\Anaconda2\python.exe (
	C:\ProgramData\Anaconda2\python.exe setVersion.py %CONFIGFILE% %*
)

del %ZIPFILE%
copy %CONFIGFILE% %CONFIGFILECOPY%
powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::CreateFromDirectory('www', 'build.zip'); }"
del %CONFIGFILECOPY%