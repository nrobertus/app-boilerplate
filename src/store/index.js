// libs
import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

// Modules
import app from './modules/app'

Vue.use(Vuex)
// Local storage
const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: window.localStorage,
  reducer: state => ({
    app: state.app
  })
})

// Vuex Store
export default new Vuex.Store({
  modules: {
    app
  },
  plugins: [vuexLocalStorage.plugin]
})
