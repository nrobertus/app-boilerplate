const state = {
  appIsLoading: false
}

const mutations = {
  setAppIsLoading (state, status) {
    state.appIsLoading = status
  }
}

const getters = {
  appIsLoading: state => state.appIsLoading
}

export default {
  state,
  mutations,
  getters
}
