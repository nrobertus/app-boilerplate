import Vue from 'vue'
import Router from 'vue-router'
import Tab1 from '@/pages/Tab1'
import Tab2 from '@/pages/Tab2'
import Tab3 from '@/pages/Tab3'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/Tab2',
    },
    {
      path: '/Tab1',
      name: 'Tab1',
      component: Tab1
    },
    {
      path: '/Tab2',
      name: 'Tab2',
      component: Tab2
    },
    {
      path: '/Tab3',
      name: 'Tab3',
      component: Tab3
    },
  ]
})

export default router
