// Import styles
import '@ionic/core/css/core.css'
import '@ionic/core/css/ionic.bundle.css'

// Import libs
import Vue from 'vue'
import IonicVue from '@ionic/vue'

// Icon stuff
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faCoffee, faLayerGroup, faHeartbeat, faPlus, faCalendarDay, faUser, faBolt, faPaperPlane } from '@fortawesome/free-solid-svg-icons'
library.add(faCoffee, faLayerGroup, faHeartbeat, faPlus, faCalendarDay, faUser, faBolt, faPaperPlane)
Vue.component('font-awesome-icon', FontAwesomeIcon)

// Import components
import App from './App.vue'
import router from './router'
import store from './store'
Vue.config.productionTip = false

// Use libs
Vue.use(IonicVue)

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
