# Mobile App Boilerplate

## This project is a starter kit for mobile apps, built on the following technologies: 
* Vue
  * Vuex
  * Vuex-persist (Used to keep Vuex values in local storage)
  * Vue-routerC;
* Ionic 4
* Sass
* Font-Awesome
* Webpack
* Babel
* Cordova (Used to build a generate installable files)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your tests
```
yarn test
```

### Lints and fixes files
```
yarn lint
```

### Serve live-reloaded app on an Android device
Note that this works on a device connected with ADB, whether over USB or TCPIP
```
yarn cordova-serve-android
```

### Prepare Cordova zip file
```
yarn cordova-zip
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
