module.exports = {
  publicPath: '',
  pluginOptions: {
    cordovaPath: 'src-cordova'
  },
  css: {
    loaderOptions: {
      sass: {
        data: `
          @import "@/scss/_mixins.scss";
          @import "@/scss/_transitions.scss";
          @import "@/scss/_variables.scss";
        `
      }
    }
  }
}
